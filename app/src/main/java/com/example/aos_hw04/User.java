package com.example.aos_hw04;

import java.io.Serializable;
import java.util.PrimitiveIterator;

public class User implements Serializable {

    private String Email;
    private String DisplayName;

    public User(String email, String displayName) {
        Email = email;
        DisplayName = displayName;
    }

    public String getEmail() {
        return Email;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    @Override
    public String toString() {
        return "User{" +
                "Email='" + Email + '\'' +
                ", DisplayName='" + DisplayName + '\'' +
                '}';
    }
}
