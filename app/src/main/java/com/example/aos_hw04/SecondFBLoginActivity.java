package com.example.aos_hw04;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

public class SecondFBLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_fblogin);

        User user = (User) getIntent().getSerializableExtra("my_user");
        findViewById(R.id.button_logout).setOnClickListener(view -> {
            FirebaseAuth.getInstance().signOut();
            finish();
        });

    }
}